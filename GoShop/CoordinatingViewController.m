//
//  CoordinatingViewController.m
//  GoShop
//
//  Created by daihua ye on 3/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CoordinatingViewController.h"

@interface CoordinatingViewController ()

@end

@implementation CoordinatingViewController
@synthesize goshopDatabase = _goshopDatabase;

static CoordinatingViewController *sharedCoordinator = nil;

#pragma mark - CoordinatingController Singleton Implementation
+ (CoordinatingViewController *)sharedInstance {
    if (sharedCoordinator == nil) {
        sharedCoordinator = [[super allocWithZone:NULL] init];
        
        // initialzie the first view controller
//        [sharedCoordinator initialize];
    }
    
    return sharedCoordinator;
}

+ (id) allocWithZone:(NSZone *)zone
{
    return [[self sharedInstance] retain];
}

- (id) copyWithZone:(NSZone*)zone
{
    return self;
}

- (id) retain
{
    return self;
}

- (NSUInteger) retainCount
{
    return NSUIntegerMax;
}

- (void) release
{
    // do nothing
}

- (id) autorelease
{
    return self;
}

@end
