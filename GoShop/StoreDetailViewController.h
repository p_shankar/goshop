//
//  StoreDetailViewController.h
//  GoShop
//
//  Created by daihua ye on 3/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Store.h"
@interface StoreDetailViewController : UITableViewController{
    NSString* address;
    NSString* phoneNumber;
    NSString* website;
}
@property (nonatomic, copy) NSString* address;
@property (nonatomic, copy) NSString* phoneNumber;
@property (nonatomic, copy) NSString* website;
@end
