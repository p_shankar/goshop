//
//  IndividualSubviewsBasedTableViewCell.m
//  GoShop
//
//  Created by daihua ye on 3/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "IndividualSubviewsBasedTableViewCell.h"

@implementation IndividualSubviewsBasedTableViewCell

-(void)setBackgroundColor:(UIColor *)backgroundColor {
    [super setBackgroundColor:backgroundColor];
    
    nameForProduct.backgroundColor = backgroundColor;
    priceForProduct.backgroundColor = backgroundColor;
    dateForProduct.backgroundColor = backgroundColor;
    companyForProduct.backgroundColor = backgroundColor;
}

-(void)setName:(NSString *)newName {
    [super setName:newName];
    nameForProduct.text = newName;
}


-(void)setPrice:(NSString *)newPrice {
    [super setPrice:newPrice];
    priceForProduct.text = [NSString stringWithFormat:@"%@", newPrice];
}

-(void)setDate:(NSString *)newDate {
    [super setDate:newDate];
    dateForProduct.text = newDate;
}

-(void)setCompany:(NSString *)newCompany {
    [super setCompany:newCompany];
    companyForProduct.text = newCompany;
}


@end
