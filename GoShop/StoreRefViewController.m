//
//  StoreRefViewController.m
//  GoShop
//
//  Created by Viet Trinh on 2/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "StoreRefViewController.h"
#import "StoreDetailViewController.h"
#import "Store.h"

@implementation StoreRefViewController
@synthesize searchBar;
@synthesize mapView;
@synthesize savedSearchTerm, savedScopeButtonIndex, filteredListContent, searchWasActive, listContent, showsUserLocation, location;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        cur_value = 8.0;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    mapView.showsUserLocation = YES;
    storelist = [[[DataController alloc] init] storeList];
    mapView.delegate = self;
    // create a filtered list that will contain products for the search results table.
	self.filteredListContent = [NSMutableArray arrayWithCapacity:[storelist count]];
	
	// restore search settings if they were saved in didReceiveMemoryWarning.
    if (self.savedSearchTerm)
	{
        [self.searchDisplayController setActive:self.searchWasActive];
        [self.searchDisplayController.searchBar setSelectedScopeButtonIndex:self.savedScopeButtonIndex];
        [self.searchDisplayController.searchBar setText:savedSearchTerm];
        self.savedSearchTerm = nil;
    }
	location = [[[mapView userLocation] location] coordinate];  
    if (location.latitude == 0.0 && location.longitude == 0.0){
        location.latitude =  38.546422;
        location.longitude =  -121.7394444;
    }
    NSLog(@"ViewDidLoad: %f, %f", location.latitude, location.longitude);
    [self search:location];
    
	mapView.mapType = MKMapTypeStandard;
    MKCoordinateSpan span = {.latitudeDelta = 0.08, .longitudeDelta =  0.08};
    MKCoordinateRegion region = {location, span};
    [mapView setRegion:region];
    
}


- (void)viewDidUnload
{
   
    [self setMapView:nil];
    [self setSearchBar:nil];
    searchBar = nil;
    [super viewDidUnload];
    self.filteredListContent = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	//return YES;
    return NO;
}

-(CLLocationCoordinate2D) getLocationFromAddressString:(NSString*) addressStr {
    NSString *urlStr = [NSString stringWithFormat:@"http://maps.google.com/maps/geo?q=%@&output=csv", 
                        [addressStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSString *locationStr = [NSString stringWithContentsOfURL:[NSURL URLWithString:urlStr] encoding:NSUTF8StringEncoding error:nil];
    NSArray *items = [locationStr componentsSeparatedByString:@","];
    
    CLLocationCoordinate2D ilocation;
    
    if([items count] >= 4 && [[items objectAtIndex:0] isEqualToString:@"200"] && [addressStr length] == 5) {
        ilocation.latitude = [[items objectAtIndex:2] doubleValue];
        ilocation.longitude = [[items objectAtIndex:3] doubleValue];
    }
    else {
        NSLog(@"Address, %@ not found: Error %@",addressStr, [items objectAtIndex:0]);
        ilocation.latitude = 0.0;
        ilocation.longitude = 0.0;
    }
    return ilocation;
}

- (void)viewDidDisappear:(BOOL)animated
{
    // save the state of the search UI so that it can be restored if the view is re-created
    self.searchWasActive = [self.searchDisplayController isActive];
    self.savedSearchTerm = [self.searchDisplayController.searchBar text];
    self.savedScopeButtonIndex = [self.searchDisplayController.searchBar selectedScopeButtonIndex];
}

-(void)search:(CLLocationCoordinate2D)searchCoordinates
{
    if (searchCoordinates.latitude != 0.0 && searchCoordinates.longitude != 0.0) {
        MKCoordinateSpan span = {.latitudeDelta = 0.02, .longitudeDelta =  0.02};
        MKCoordinateRegion region = {searchCoordinates, span};
        [mapView setRegion:region animated:YES];
        [mapView regionThatFits:region];
        [mapView setCenterCoordinate:searchCoordinates animated:YES];
        mapView.delegate = self;
        CLLocationCoordinate2D ilocation;
        NSMutableArray* annotations = [[NSMutableArray alloc] init];
        int i;
        for(i = 0; i < [storelist count]; i++){
            ilocation.latitude = [[storelist objectAtIndex:i] latitude];
            ilocation.longitude = [[storelist objectAtIndex:i] longitude];
            Store* store = [storelist objectAtIndex:i];
            NSString* currentAddress = [NSString stringWithFormat:@"%@ %@, %@", [store streetNumber], [store streetName], [store city]];
            MyAnnotation* myAnnotation=[[MyAnnotation alloc] init:ilocation:@"Safeway":currentAddress:store];
            [mapView addAnnotation:myAnnotation];
            [annotations addObject:myAnnotation];
        }
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)theMapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    // If it's the user location, just return nil.
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    // Handle any custom annotations.
    if ([annotation isKindOfClass:[MyAnnotation class]])
    {
        // Try to dequeue an existing pin view first.
        MKPinAnnotationView* pinView = (MKPinAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"CustomPinAnnotationView"];
        
        if (!pinView)
        {
            // If an existing pin view was not available, create one.
            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"CustomPinAnnotation"];
            pinView.pinColor = MKPinAnnotationColorRed;
            pinView.animatesDrop = YES;
            pinView.canShowCallout = YES;
            
            // Add a detail disclosure button to the callout.
            UIButton* rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            pinView.rightCalloutAccessoryView = rightButton;
        }
        else
            pinView.annotation = annotation;
        return pinView;
    }
    return nil;
}

- (void)mapView:(MKMapView *)mv annotationView:(MKAnnotationView *)pin calloutAccessoryControlTapped:(UIControl *)control 
{
    StoreDetailViewController *controller = [[StoreDetailViewController alloc] init];
    controller.address = [[NSString alloc] initWithFormat:@"%@ %@, %@", [[(MyAnnotation*)[pin annotation] store] streetNumber], [[(MyAnnotation*)[pin annotation] store] streetName], [[(MyAnnotation*)[pin annotation] store] city]];
    controller.phoneNumber = [[(MyAnnotation*)[pin annotation] store] phoneNumber];
    controller.website = [[[(MyAnnotation*)[pin annotation] store] website] absoluteString];
    
    [[self navigationController] pushViewController:controller animated:YES];
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
	/*
	 Update the filtered array based on the search text and scope.
	 */
	
	[self.filteredListContent removeAllObjects]; // First clear the filtered array.
	
	/*
	 Search the main list for products whose type matches the scope (if selected) and whose name matches searchText; add items that match to the filtered array.
	 */
    for (Store *store in storelist)
	{
		if ([scope isEqualToString:@"ZipCode"] || [store.zipCode isEqualToString:scope])
		{
			NSComparisonResult result = [store.zipCode compare:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [searchText length])];
            if (result == NSOrderedSame){
				[self.filteredListContent addObject:store];
            }
		}
        else if ([scope isEqualToString:@"City"] || [store.city isEqualToString:scope])
		{
			NSComparisonResult result = [store.city compare:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [searchText length])];
            if (result == NSOrderedSame){
				[self.filteredListContent addObject:store];
            }
		}
        else if ([scope isEqualToString:@"Street Name"] || [store.streetName isEqualToString:scope])
		{
			NSComparisonResult result = [store.streetName compare:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [searchText length])];
            if (result == NSOrderedSame){
				[self.filteredListContent addObject:store];
            }
		}
	}
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption
{
    savedScopeButtonIndex = searchOption;
    [self filterContentForSearchText:[self.searchDisplayController.searchBar text] scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]]; 
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIViewController *detailsViewController = [[UIViewController alloc] init];
    Store* store = [self.filteredListContent objectAtIndex:indexPath.row];
	detailsViewController.title = store.zipCode;
    
    CLLocationCoordinate2D ilocation;
    ilocation.latitude = store.latitude;
    ilocation.longitude = store.longitude;
    mapView.mapType = MKMapTypeStandard;
    MKCoordinateSpan span = {.latitudeDelta = 0.08, .longitudeDelta =  0.08};
    MKCoordinateRegion region = {ilocation, span};
    [mapView setRegion:region];
    [self.view endEditing:YES];
    [tableView removeFromSuperview];
    searchBar.text = nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *kCellID = @"cellID";
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
	if (cell == nil)
	{
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:kCellID];
		cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	}
	Store *store;
	store = [self.filteredListContent objectAtIndex:indexPath.row];
    if(savedScopeButtonIndex == 0){
        cell.textLabel.text = store.zipCode;
    }
    if(savedScopeButtonIndex == 1){
        cell.textLabel.text = store.city;
    }
    if(savedScopeButtonIndex == 2){
        cell.textLabel.text = store.streetName;
    }
    cell.detailTextLabel.text = [[NSString alloc] initWithFormat:@"%@ %@, %@", store.streetNumber, store.streetName, store.city];
    
	return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [self.filteredListContent count];
}


@end
