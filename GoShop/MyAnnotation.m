#import "MyAnnotation.h"

@implementation MyAnnotation
-(MyAnnotation*) init:(CLLocationCoordinate2D)myCoordinate:(NSString*)myTitle:(NSString*)mySubtitle:(Store*)myStore{
    coordinate = myCoordinate;
    title = myTitle;
    subtitle = mySubtitle;
    store = myStore;
    return self;
}
@synthesize title;
@synthesize subtitle;
@synthesize coordinate;
@synthesize store;
@end
