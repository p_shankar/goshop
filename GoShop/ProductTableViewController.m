//
//  ProductTableViewController.m
//  GoShop
//
//  Created by daihua ye on 3/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ProductTableViewController.h"
#import "Product.h"
#import "Product+Create.h"
#import "GoShopData.h"
#import "ProductDetailTableViewController.h"

/*
 Predefined colors to alternate the background color of each cell row by row
 (see tableView:cellForRowAtIndexPath: and tableView:willDisplayCell:forRowAtIndexPath:).
 */
//#define DARK_BACKGROUND  [UIColor colorWithRed:255.0/255.0 green:248.0/255.0 blue:220.0/255.0 alpha:1.0]
//#define LIGHT_BACKGROUND [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0]

@implementation ProductTableViewController

@synthesize category = _category;
@synthesize productDocument = _productDocument;
@synthesize tmpCell, cellNib;

@synthesize listContent, filteredListContent, savedSearchTerm, savedScopeButtonIndex, searchWasActive;

#pragma mark - Setup for Category
-(void)setCategory:(NSDictionary *)categoryWithDocument {
    _category = [categoryWithDocument objectForKey:@"category"];
    _productDocument = [categoryWithDocument objectForKey:@"product_database"];
    self.title = _category.name;
    [self setupFetchedResultsController];
}


-(void) createProductsWithCategoryId:(NSNumber *)category_id inManagedDocument:(UIManagedDocument *)document {
    dispatch_queue_t fetchQ = dispatch_queue_create("Product fetcher", NULL);
    dispatch_async(fetchQ, ^{
        NSArray *products = [GoShopData recentGeoreferenced:@"product" andQuery:[NSString stringWithFormat:@"categories/%d/products", [category_id intValue]]];
//        NSLog(@"products is %@", [products objectAtIndex:0]);
        [document.managedObjectContext performBlock:^{
            for (NSDictionary *productInfo in products) {
                if ([[productInfo objectForKey:GOSHOP_PRODUCT_NAME] isEqualToString:@""] == false) {
                    [Product createProductWithCategoryID:productInfo andCategory:self.category inManagedObjectContext:document.managedObjectContext];                    
                }
            }
            [document saveToURL:document.fileURL forSaveOperation:UIDocumentSaveForOverwriting completionHandler:^(BOOL success) {
                if (success) {
                    // after finishing the saving, create the search
                    [self setUpSearchBar];
                } else {
                    NSLog(@"Couldn't create document at %@", document.fileURL);
                }
            }];
        }];
    });
    dispatch_release(fetchQ);
}

-(void) setupFetchedResultsController {
    NSLog(@"set up for product");
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Product"];
    request.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"name"
                                                                                     ascending:YES
                                                                                      selector:@selector(localizedCaseInsensitiveCompare:)]];
    request.predicate = [NSPredicate predicateWithFormat:@"category_id = %@", self.category.category_id];
    
    NSError *error = nil;
    NSArray *matches = [self.productDocument.managedObjectContext executeFetchRequest:request error:&error];
    
    NSLog(@"matches's category id is %d", [self.category.category_id intValue]);
    NSLog(@"matches's products has %d", [matches count]);
//    NSLog(@"matech's categories id of mateches is %@", [[matches objectAtIndex:2] category_id]);
    if (matches.count == 0 && self.productDocument.documentState == UIDocumentStateNormal) {
        [self createProductsWithCategoryId:self.category.category_id inManagedDocument:self.productDocument];
    } else {
        [self setUpSearchBarWithProducts:matches];
    }
    
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request
                                                                        managedObjectContext:self.category.managedObjectContext
                                                                          sectionNameKeyPath:nil
                                                                                   cacheName:nil];
}

#pragma mark - SetUp Search Bar
-(void) setUpSearchBar {
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Product"];
    request.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"name"
                                                                                     ascending:YES
                                                                                      selector:@selector(localizedCaseInsensitiveCompare:)]];
    request.predicate = [NSPredicate predicateWithFormat:@"category_id = %@", self.category.category_id];
    
    NSError *error = nil;
    NSArray *matches = [self.productDocument.managedObjectContext executeFetchRequest:request error:&error];
    
    [self setUpSearchBarWithProducts:matches];
}

-(void) setUpSearchBarWithProducts:(NSArray *) products {
	// create a filtered list that will contain products for the search results table.
    self.listContent = products;
    
	self.filteredListContent = [NSMutableArray arrayWithCapacity:[self.listContent count]];
	
	// restore search settings if they were saved in didReceiveMemoryWarning.
    if (self.savedSearchTerm)
	{
        [self.searchDisplayController setActive:self.searchWasActive];
        [self.searchDisplayController.searchBar setSelectedScopeButtonIndex:self.savedScopeButtonIndex];
        [self.searchDisplayController.searchBar setText:savedSearchTerm];
        
        self.savedSearchTerm = nil;
    }
	
	[self.tableView reloadData];
	self.tableView.scrollEnabled = YES;
}

#pragma mark - Table View Source

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        static NSString *cellID = @"result";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        Product *product = [self.filteredListContent objectAtIndex:indexPath.row];
        
        cell.textLabel.text = product.name;
        cell.textLabel.lineBreakMode = UILineBreakModeWordWrap;
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ between: %@", product.item_price, product.price_valid];
        return cell;
    } else {
        static NSString *CellIdentifier = @"Product Cell";
        TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];        
        if (cell == nil) {
            [self.cellNib instantiateWithOwner:self options:nil];
            cell = tmpCell;
        }
        
        Product *product = [self.listContent objectAtIndex:indexPath.row];
        //cell.useDarkBackground = (indexPath.row %2 == 0);
        
        cell.name = product.name;
        cell.price = product.item_price;
        cell.date = product.price_valid;
        return cell;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
//    NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
//    Product *product = [self.fetchedResultsController objectAtIndexPath:indexPath]; // ask NSFRC for the NSMO at the row in question
//    if ([segue.destinationViewController respondsToSelector:@selector(setPhotographer:)]) {
//        // use performSelector:withObject: to send without compiler checking
//        // (which is acceptable here because we used introspection to be sure this is okay)
//        [segue.destinationViewController performSelector:@selector(setPhotographer:) withObject:product];
//    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	/*
	 If the requesting table view is the search display controller's table view, return the count of
     the filtered list, otherwise return the count of the main list.
	 */
	if (tableView == self.searchDisplayController.searchResultsTableView)
	{
        return [self.filteredListContent count];
    }
	else
	{
        NSLog(@"in number of rows, listContent is %d", [self.listContent count]);
//        return  [self.listContent count];
        return [[[self.fetchedResultsController sections] objectAtIndex:section] numberOfObjects];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if (tableView != self.searchDisplayController.searchResultsTableView)
//        cell.backgroundColor = ((TableViewCell *)cell).useDarkBackground ? DARK_BACKGROUND : LIGHT_BACKGROUND;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    ProductDetailTableViewController *controller = [[ProductDetailTableViewController alloc] init];

    Product *product = nil;
	if (tableView == self.searchDisplayController.searchResultsTableView)
	{
        product = [self.filteredListContent objectAtIndex:indexPath.row];
    }
	else
	{
        product = [self.listContent objectAtIndex:indexPath.row];
    }
	controller.title = product.name;
    controller.product = product;
    
    [[self navigationController] pushViewController:controller animated:YES];
}

#pragma mark -
#pragma mark Content Filtering

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
	/*
	 Update the filtered array based on the search text and scope.
	 */
	
	[self.filteredListContent removeAllObjects]; // First clear the filtered array.
	
	/*
	 Search the main list for products whose type matches the scope (if selected) and whose name matches searchText; add items that match to the filtered array.
	 */
	for (Product *product in listContent)
	{
//		if ([scope isEqualToString:@"All"] || [product.type isEqualToString:scope])
//		{
			NSComparisonResult result = [product.name compare:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [searchText length])];
            if (result == NSOrderedSame)
			{
				[self.filteredListContent addObject:product];
            }
//		}
	}
}


#pragma mark -
#pragma mark UISearchDisplayController Delegate Methods

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}


- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption
{
    [self filterContentForSearchText:[self.searchDisplayController.searchBar text] scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
    
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

#pragma mark - View Cycle

-(void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"view Did Load");
    
    // Configure the table view.
    self.tableView.rowHeight = 73.0;
    //self.tableView.backgroundColor = DARK_BACKGROUND;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
                        
    self.cellNib = [UINib nibWithNibName:@"IndividualTableViewCell" bundle:nil];
}

- (void)viewDidUnload {
    [self setTmpCell:nil];
    [super viewDidUnload];
}
@end
