//
//  Store.h
//  GoShop
//
//  Created by Ricardo Nunez on 3/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface Store : NSObject{
    NSString* name;
    NSString* identifier;
    NSString* streetNumber;
    NSString* streetName;
    NSString* city;
    NSString* zipCode;
    float latitude;
    float longitude;
    NSString* phoneNumber;
    NSURL* website;
}
-(Store*)init:(NSString*)myName:(NSString*)myIdentifier:(NSString*)myStreetNumber:(NSString*)myStreetName:(NSString*)myCity:(NSString*)myAddress:(NSString*)myZipCode:(float)myLatitude:(float)myLongitude:(NSString*)myPhoneNumber:(NSURL*)myWebsite;
@property (nonatomic, copy) NSString* name;
@property (nonatomic, copy) NSString* identifier;
@property (nonatomic, copy) NSString* streetNumber;
@property (nonatomic, copy) NSString* streetName;
@property (nonatomic, copy) NSString* city;
@property (nonatomic, copy) NSString* zipCode;
@property (nonatomic) float latitude;
@property (nonatomic) float longitude;
@property (nonatomic, copy) NSString* phoneNumber;
@property (nonatomic, copy) NSURL* website;
@end
