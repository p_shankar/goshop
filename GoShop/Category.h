//
//  Category.h
//  GoShop
//
//  Created by daihua ye on 3/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Product;

@interface Category : NSManagedObject

@property (nonatomic, retain) NSNumber * category_id;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * number;
@property (nonatomic, retain) NSSet *hasProducts;
@end

@interface Category (CoreDataGeneratedAccessors)

- (void)addHasProductsObject:(Product *)value;
- (void)removeHasProductsObject:(Product *)value;
- (void)addHasProducts:(NSSet *)values;
- (void)removeHasProducts:(NSSet *)values;

@end
