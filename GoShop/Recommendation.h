//
//  Recommendation.h
//  GoShop
//
//  Created by daihua ye on 3/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Product;

@interface Recommendation : NSManagedObject

@property (nonatomic, retain) NSNumber * rate;
@property (nonatomic, retain) NSString * message;
@property (nonatomic, retain) NSString * image_url;
@property (nonatomic, retain) NSNumber * product_id;
@property (nonatomic, retain) NSNumber * recommendation_id;
@property (nonatomic, retain) Product *whichProduct;

@end
