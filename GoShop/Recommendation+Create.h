//
//  Recommendation+Create.h
//  GoShop
//
//  Created by daihua ye on 3/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Recommendation.h"

@interface Recommendation (Create)

+(Recommendation *)createRecommendationWithInfo:(NSDictionary *)recommendationInfo inManagedObjectContext:(NSManagedObjectContext *)context;

@end
