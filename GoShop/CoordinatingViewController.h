//
//  CoordinatingViewController.h
//  GoShop
//
//  Created by daihua ye on 3/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CoordinatingViewController : UIViewController

@property (nonatomic, strong) UIManagedDocument *goshopDatabase;

+(CoordinatingViewController *) sharedInstance;
@end
