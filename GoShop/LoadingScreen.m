//
//  LoadingScreen.m
//  GoShop
//
//  Created by Viet Trinh on 3/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LoadingScreen.h"

@implementation LoadingScreen

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        self.backgroundColor = [UIColor blackColor];
        self.alpha =0.8;
        UIActivityIndicatorView* spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [spinner setCenter:CGPointMake(100, 85)];
        
        UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(60, 120, 100, 20)];
        [label setText:@"Loading..."];
        [label setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20]];
        [label setTextColor:[UIColor whiteColor]];
        [label setBackgroundColor:[UIColor colorWithWhite:0 alpha:0]];
        
        [self addSubview:spinner];
        [self addSubview:label];
        [spinner startAnimating];
                                       
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
