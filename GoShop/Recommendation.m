//
//  Recommendation.m
//  GoShop
//
//  Created by daihua ye on 3/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Recommendation.h"
#import "Product.h"


@implementation Recommendation

@dynamic rate;
@dynamic message;
@dynamic image_url;
@dynamic product_id;
@dynamic recommendation_id;
@dynamic whichProduct;

@end
