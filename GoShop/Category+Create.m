//
//  Category+Create.m
//  GoShop
//
//  Created by daihua ye on 3/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Category+Create.h"
#import "GoShopData.h"

@implementation Category (Create)

//+(Category *)categoryWithName:(NSNumber *)category_id inManagedObjectContext:(NSManagedObjectContext *)context {
//    Category *category = nil;
//    
//    NSArray *categories = [GoShopData recentGeoreferenced:@"category" andQuery:@"categories"];
//    
//    NSString *name;
//    NSNumber *number;
//    for (NSDictionary *d in categories) {
//        if ([category_id compare: ((NSNumber *)[d objectForKey:@"id"])] == NSOrderedSame) {
//            name = [d objectForKey:GOSHOP_CATEGORY_NAME];
//            number = [d objectForKey:GOSHOP_CATEGORY_NUMBER];
//            break;
//        }
//    }
//    
//    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Category"];
//    request.predicate = [NSPredicate predicateWithFormat:@"name = %@", name];
//    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
//    request.sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
//    
//    NSError *error = nil;
//    NSArray *match_categories = [context executeFetchRequest:request error:&error];
//    
//    if (!match_categories || ([match_categories count] > 1)) {
//        // handle error
//    } else if (![match_categories count]) {
//        category = [NSEntityDescription insertNewObjectForEntityForName:@"Category" inManagedObjectContext:context];
//        category.name = name;
//        category.number = number;
//        category.category_id = category_id;
//    } else {
//        category = [match_categories lastObject];
//    }
//    
//    return category;
//}

+(Category *)createCategoryWithCategories:(NSDictionary *)category inManagedObjectContext:(NSManagedObjectContext *)context {
    
    Category *category_return = nil;
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Category"];
    request.predicate = [NSPredicate predicateWithFormat:@"name = %@", [category objectForKey:GOSHOP_CATEGORY_NAME]];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    request.sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    NSError *error = nil;
    NSArray *match_categories = [context executeFetchRequest:request error:&error];
    
    if (!match_categories || ([match_categories count] > 1)) {
        // handle error
    } else if (![match_categories count] && [category objectForKey:GOSHOP_CATEGORY_NAME] != (id)[NSNull null]) {
        category_return = [NSEntityDescription insertNewObjectForEntityForName:@"Category" inManagedObjectContext:context];
        category_return.name = [category objectForKey:GOSHOP_CATEGORY_NAME];
        category_return.number = [category objectForKey:GOSHOP_CATEGORY_NUMBER];
        category_return.category_id = [category objectForKey:GOSHOP_CATEGORY_ID];
    } else {
        if ([[match_categories lastObject] category_id] != [category objectForKey:GOSHOP_CATEGORY_ID] && [category objectForKey:GOSHOP_CATEGORY_NAME] != (id)[NSNull null] && [[category objectForKey:GOSHOP_CATEGORY_NUMBER] intValue] > 0) {
//            category_return = [NSEntityDescription insertNewObjectForEntityForName:@"Category" inManagedObjectContext:context];
            category_return = [match_categories lastObject];
            category_return.name = [category objectForKey:GOSHOP_CATEGORY_NAME];
            category_return.number = [category objectForKey:GOSHOP_CATEGORY_NUMBER];
            category_return.category_id = [category objectForKey:GOSHOP_CATEGORY_ID];
//            NSLog(@"here is the match categoryies %@", [category_return name]);
        } else {
            category_return = [match_categories lastObject];
            category_return.number = 0;
            category_return.category_id = 0;
        }
    }
    return category_return;
}

@end
