//
//  DataController.h
//  GoShop
//
//  Created by Ricardo Nunez on 3/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#include "Store.h"
@interface DataController : NSObject{
    NSMutableArray* storeList;
}
@property (readonly, copy) NSMutableArray* storeList;
@end
