//
//  AddRecommendationTableViewController.m
//  GoShop
//
//  Created by daihua ye on 3/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AddRecommendationTableViewController.h"
#import "RecommendationTableViewController.h"
#import "ASIFormDataRequest.h"
#import "GoShopData.h"

@interface AddRecommendationTableViewController ()

- (void)uploadFailed:(ASIHTTPRequest *)theRequest;
- (void)uploadFinished:(ASIHTTPRequest *)theRequest;

@end

@implementation AddRecommendationTableViewController
@synthesize tmpCell, cellNib;
@synthesize request = _request;
@synthesize tmpArray = _tmpArray;
@synthesize rateView;
@synthesize editView;
@synthesize message = _message;
@synthesize rateForProduct = _rateForProduct;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.rowHeight = 169.0;
    self.cellNib = [UINib nibWithNibName:@"AddRecommendationViewCell" bundle:nil];
}
- (void)createRateView
{    
    self.rateView.notSelectedImage = [UIImage imageNamed:@"empty_star.png"];
    self.rateView.fullSelectedImage = [UIImage imageNamed:@"full_star.png"];
 
    self.rateView.rating = 0;
    self.rateView.editable = YES;
    self.rateView.maxRating = 5;
    self.rateView.delegate = self;
    self.message = [[NSString alloc] init];
    self.rateForProduct = [[NSNumber alloc] initWithFloat:0];
    self.editView.delegate = self;
    
    NSLog(@"is Recommendation what is rateView now %@", self.rateView.notSelectedImage);
  
}

- (void)viewDidUnload
{
    [super viewDidUnload];
//    [self setRateView:nil];
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    //return YES;
    return NO;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Add Recommendation Cell";
    AddRecommendationCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        _tmpArray = [self.cellNib instantiateWithOwner:self options:nil];
        cell = tmpCell;
    }
    [self createRateView];
        NSLog(@"cell for row at Index");
    // Configure the cell...
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}

#pragma mark - Handle the Navigation Action

- (IBAction)cancelRecommendation:(id)sender {
    RecommendationTableViewController *recommendationController = [[RecommendationTableViewController alloc] init];
    [self dismissModalViewControllerAnimated:YES];
    
    [[self navigationController] pushViewController:recommendationController animated:YES];
}

- (IBAction)saveRecommendation:(id)sender {    
    
    [_request cancel];
    
    NSLog(@"message is %@", self.message);
    NSLog(@"rate is %@", self.rateForProduct);
    
    [self setRequest:[ASIFormDataRequest requestWithURL:[NSURL URLWithString:GOSHOP_RECOMMENDATION_POST_URL]]];
    [_request setPostValue:self.rateForProduct forKey:@"rate"];
    [_request setPostValue:self.message forKey:@"message"];
    [_request setPostValue:@"http://goshopucd.heroku.com" forKey:@"url"];
    [_request setTimeOutSeconds:20];
    
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_4_0
    [_request setShouldContinueWhenAppEntersBackground:YES];
#endif
	[_request setDelegate:self];
	[_request setDidFailSelector:@selector(uploadFailed:)];
	[_request setDidFinishSelector:@selector(uploadFinished:)];
    
    [_request startAsynchronous];    
}

- (void)uploadFailed:(ASIHTTPRequest *)theRequest
{
    
}
- (void)uploadFinished:(ASIHTTPRequest *)theRequest
{
    RecommendationTableViewController *controller = [[RecommendationTableViewController alloc] init];
    [self dismissModalViewControllerAnimated:YES];
    [[self navigationController] pushViewController:controller animated:YES];
    
}

-(void)rateView:(RateView *)rateView ratingDidChange:(float)rating {
    self.rateForProduct = [[NSNumber alloc] initWithFloat:rating];
}

-(void)textViewDidBeginEditing:(UITextView *)textView {
    textView.text = nil;
}

-(void)textViewDidEndEditing:(UITextView *)textView {
}

-(void)textViewDidChange:(UITextView *)textView {
    self.message = textView.text;
}


@end
