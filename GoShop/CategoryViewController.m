//
//  CategoryViewController.m
//  GoShop
//
//  Created by Viet Trinh on 2/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CategoryViewController.h"
#import "GoShopData.h"
#import "Product+Create.h"
#import "Category+Create.h"
#import "Product.h"
#import "Category.h"
#import <QuartzCore/QuartzCore.h>

@implementation CategoryViewController

@synthesize productDatabase = _productDatabase;
@synthesize delegate = _delegate;

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}



#pragma mark - View lifecycle

-(void)viewWillAppear:(BOOL)animated {
    //[super viewWillAppear:animated];
    [super viewWillAppear:animated];
    // Start loading screen
    loadScreen = [[LoadingScreen new] initWithFrame:CGRectMake(60, 110, 200, 200)];
    loadScreen.layer.cornerRadius = 30.0f;
    [self.view addSubview:loadScreen];

    // Purpose: Create a default database if none is set in the internal memory
    if (!self.productDatabase) {  
        NSURL *url = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
        url = [url URLByAppendingPathComponent:@"Products1"];
        NSLog(@"%@", url);
        // url to the internal memory is "<Documents Directory>/Products"
        self.productDatabase = [[UIManagedDocument alloc] initWithFileURL:url]; // setter will create this for us on disk
    }
    else{
        [loadScreen removeFromSuperview];
    }
   
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - GoShop Database
-(void)setProductDatabase:(UIManagedDocument *)productDatabase {
   if (_productDatabase != productDatabase) {
        _productDatabase = productDatabase;
       
       CoordinatingViewController *coordinator = [CoordinatingViewController sharedInstance];
       coordinator.goshopDatabase = productDatabase;
       
        [self useDocument];
    }
    //[loadScreen removeFromSuperview];
}

- (void)useDocument
{
    NSLog(@"ProductDatabase state is %d", self.productDatabase.documentState);
    [self.delegate categoryViewController:self inManagedDocumentContext:self.productDatabase];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:[self.productDatabase.fileURL path]]) {
        // file containing database does not exist on disk, so create it
        [self.productDatabase saveToURL:self.productDatabase.fileURL forSaveOperation:UIDocumentSaveForCreating completionHandler:^(BOOL success) {
            [self setupFetchedResultsController];
            [self fetchServerDataIntoDocument:self.productDatabase];
        }];
        
    } else if (self.productDatabase.documentState == UIDocumentStateClosed) {
        // file containing database exists on disk, but we need to open it
        [loadScreen removeFromSuperview];
        [self.productDatabase openWithCompletionHandler:^(BOOL success) {
            [self setupFetchedResultsController];
            [self fetchServerDataIntoDocument:self.productDatabase];
        }];
    } else if (self.productDatabase.documentState == UIDocumentStateNormal) {
        // file containing databsase already open and ready to use
        [loadScreen removeFromSuperview];
        [self setupFetchedResultsController];
    }
}

- (void)setupFetchedResultsController // attaches an NSFetchRequest to this UITableViewController
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Category"];
    
    request.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)]];
    
    // no predicate because we want ALL the Categories
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request
                                                                        managedObjectContext:self.productDatabase.managedObjectContext
                                                                        sectionNameKeyPath:nil
                                                                        cacheName:nil];
}

- (void)fetchServerDataIntoDocument:(UIManagedDocument *)document
{
    dispatch_queue_t fetchQ = dispatch_queue_create("Category fetch", NULL);
    dispatch_async(fetchQ, ^{
        // set up the data
        NSArray *categories = [GoShopData recentGeoreferenced:@"category" andQuery:@"categories"];
        [document.managedObjectContext performBlock:^{ // perform in the NSMOC's safe thread (main thread)
            for (NSDictionary *categoryInfo in categories) {
                [Category createCategoryWithCategories:categoryInfo inManagedObjectContext:document.managedObjectContext];
            }
            [loadScreen removeFromSuperview];
            [document saveToURL:document.fileURL forSaveOperation:UIDocumentSaveForOverwriting completionHandler:NULL];
        }];
    });
    dispatch_release(fetchQ);
}

#pragma Mark - Table View function
// 14. Load up our cell using the NSManagedObject retrieved using NSFRC's objectAtIndexPath:
// (go to PhotosByPhotographerViewController.h (header file) for next step)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Category Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    Category *category = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    
    cell.textLabel.text = [category name];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%d products", [[category number] intValue]];
    
    
    NSString* imageName = [NSString stringWithFormat:@"%@.png",[category name]];
    cell.imageView.image = [UIImage imageNamed:imageName];
    
    
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
    Category *category = [self.fetchedResultsController objectAtIndexPath:indexPath];
    // be somewhat generic here (slightly advanced usage)
    // we'll segue to ANY view controller that has a photographer @property
    if ([segue.destinationViewController respondsToSelector:@selector(setCategory:)]) {
        // use performSelector:withObject: to send without compiler checking
        // (which is acceptable here because we used introspection to be sure this is okay)
        NSDictionary *categoryWithDocument = [[NSDictionary alloc] initWithObjectsAndKeys:self.productDatabase, @"product_database", category, @"category", nil];
        [segue.destinationViewController performSelector:@selector(setCategory:) withObject:categoryWithDocument];
    }
}


@end
