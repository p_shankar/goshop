//
//  IndividualSubviewsBasedTableViewCell.h
//  GoShop
//
//  Created by daihua ye on 3/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableViewCell.h"

@interface IndividualSubviewsBasedTableViewCell : TableViewCell
{
    IBOutlet UILabel *nameForProduct;
    IBOutlet UILabel *priceForProduct;
    IBOutlet UILabel *dateForProduct;
    IBOutlet UILabel *companyForProduct;
}

@end
