//
//  RecommendationTableViewController.m
//  GoShop
//
//  Created by daihua ye on 3/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RecommendationTableViewController.h"
#import "Recommendation.h"
#import "Recommendation+Create.h"
#import "GoShopData.h"
#import "CategoryViewController.h"
#import "CoordinatingViewController.h"

@interface RecommendationTableViewController ()
@end

@implementation RecommendationTableViewController
@synthesize tmpCell, cellNib;
@synthesize recommendationDatabase = _recommendationDatabase;

#pragma mark - View Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.rowHeight = 105.0;
    self.cellNib = [UINib nibWithNibName:@"RecommendationViewCell" bundle:nil];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    CoordinatingViewController *coordinator = [CoordinatingViewController sharedInstance];
    self.recommendationDatabase = coordinator.goshopDatabase;
    [self useDocument];
}

#pragma mark - Set Recommenadtion Database
-(void)setRecommendationDatabase:(UIManagedDocument *)recommendationDatabase {
    if (_recommendationDatabase != recommendationDatabase) {
        _recommendationDatabase = recommendationDatabase;
        [self setupFetchedResultsController];
    }
}

-(void) useDocument {
    NSLog(@"Recommendation state is %d", self.recommendationDatabase.documentState);
    if (![[NSFileManager defaultManager] fileExistsAtPath:[self.recommendationDatabase.fileURL path]]) {
        // file containing database does not exist on disk, so create it
        [self.recommendationDatabase saveToURL:self.recommendationDatabase.fileURL forSaveOperation:UIDocumentSaveForCreating completionHandler:^(BOOL success) {
            [self setupFetchedResultsController];
            [self fetchServerDataIntoDocument:self.recommendationDatabase];
            
        }];
    } else if (self.recommendationDatabase.documentState == UIDocumentStateClosed) {
        // file containing database exists on disk, but we need to open it
        [self.recommendationDatabase openWithCompletionHandler:^(BOOL success) {
            [self setupFetchedResultsController];
            [self fetchServerDataIntoDocument:self.recommendationDatabase];
        }];
    } else if (self.recommendationDatabase.documentState == UIDocumentStateNormal) {
        // file containing databsase already open and ready to use
        [self setupFetchedResultsController];
        [self fetchServerDataIntoDocument:self.recommendationDatabase];
    }
}

-(void)setupFetchedResultsController {
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Recommendation"];
    
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"recommendation_id" ascending:NO];
    request.sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
//    request.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"recommendation_id" ascending:NO selector:@selector(localizedCaseInsensitiveCompare:)]];
    // no predicate because we want ALL the Categories
        
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request
                                                                        managedObjectContext:self.recommendationDatabase.managedObjectContext
                                                                          sectionNameKeyPath:nil
                                                                                   cacheName:nil];
    
}

-(void) fetchServerDataIntoDocument:(UIManagedDocument *)document {
    NSLog(@"fetchServerDataIntoDocument in recommendation");
    dispatch_queue_t fetchQ = dispatch_queue_create("Recommendation fetch", NULL);
    dispatch_async(fetchQ, ^{
        NSArray *recommendations = [GoShopData recentGeoreferenced:@"recommendation" andQuery:@"recommendations"];
        [document.managedObjectContext performBlock:^{ // perform in the NSMOC's safe thread (main thread)
            for (NSDictionary *recommendationInfo in recommendations) {
                [Recommendation createRecommendationWithInfo:recommendationInfo inManagedObjectContext:document.managedObjectContext];
            }
            [document saveToURL:document.fileURL forSaveOperation:UIDocumentSaveForOverwriting completionHandler:NULL];
        }];
    });
    dispatch_release(fetchQ);
}

#pragma mark - Table view data source

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Recommendation Cell";
    RecommendationCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        [self.cellNib instantiateWithOwner:self options:nil];
        cell = tmpCell;
    }
    // Configure the cell...
    Recommendation *recommendation = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.recommendation_message = recommendation.message;
    cell.image_url = recommendation.image_url;
    UIImage *star = [UIImage imageNamed:[NSString stringWithFormat:@"%@.png", recommendation.rate]];
    cell.rateFromUser = [[UIImageView alloc] initWithImage:star];
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}


#pragma mark - Refresh the RateView
- (IBAction)refreshRecommendation:(id)sender {
    [self useDocument];
}

@end
