//
//  Product.m
//  GoShop
//
//  Created by daihua ye on 3/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Product.h"
#import "Category.h"
#import "Recommendation.h"


@implementation Product

@dynamic category_id;
@dynamic descriptions;
@dynamic item_price;
@dynamic name;
@dynamic price_valid;
@dynamic product_id;
@dynamic saving;
@dynamic whichCategory;
@dynamic hasRecommendations;

@end
