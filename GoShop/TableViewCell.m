//
//  TableViewCell.m
//  GoShop
//
//  Created by daihua ye on 3/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TableViewCell.h"

@implementation TableViewCell
@synthesize name, useDarkBackground, date, price, company;

//- (void)setUseDarkBackground:(BOOL)flag 
//{
//    if (flag != useDarkBackground || !self.backgroundView) {
//        useDarkBackground = flag;
//        NSString *backgroundImagePath = [[NSBundle mainBundle] pathForResource:useDarkBackground ? @"DarkBackground" : @"LightBackground" ofType:@"png"];
//        UIImage *backgroundImage = [[UIImage imageWithContentsOfFile:backgroundImagePath] stretchableImageWithLeftCapWidth:0.0 topCapHeight:1.0];
//        self.backgroundView = [[UIImageView alloc] initWithImage:backgroundImage];
//        self.backgroundView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
//        self.backgroundView.frame = self.bounds;    
//    }
//}

@end
