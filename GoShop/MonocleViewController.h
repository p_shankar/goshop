//
//  MonocleViewController.h
//  GoShop
//
//  Created by Viet Trinh on 3/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ARGeoViewController.h"

@interface MonocleViewController : UIViewController <ARViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIPopoverControllerDelegate>{
    UIPopoverController* imagePickerPopover;


}
- (UIView *)viewForCoordinate:(ARCoordinate *)coordinate;

@end
