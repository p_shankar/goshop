//
//  GoShopData.m
//  GoShop
//
//  Created by daihua ye on 3/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GoShopData.h"

@implementation GoShopData

+ (NSDictionary *)executeFlickrFetch:(NSString *)query {
    query = [NSString stringWithFormat:@"%@%@.json", GOSHOP_URL, query];
    query = [query stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//    NSLog(@"[%@ %@] sent %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), query);

    NSData *jsonData = [[NSString stringWithContentsOfURL:[NSURL URLWithString:query] encoding:NSUTF8StringEncoding error:nil]dataUsingEncoding:NSUTF8StringEncoding];
    NSError *error = nil;
    NSDictionary *results = jsonData ? [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&error] : nil;
    
    if (error) 
        NSLog(@"[%@ %@] JSON error: %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), error.localizedDescription);

//    NSLog(@"[%@ %@] received %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), results);
//    NSLog(@"results count is %d", [results count]);
//    NSLog(@"results is %@", [results class]);
    return results;
}

+ (NSArray *)recentGeoreferenced:(NSString *)keypath andQuery:(NSString *)query {
    // keypath format: products.product
    // query format: products
    NSString *request = [NSString stringWithFormat:query];
    
    return [[self executeFlickrFetch:request] valueForKey:keypath];
}
@end
