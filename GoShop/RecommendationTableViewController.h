//
//  RecommendationTableViewController.h
//  GoShop
//
//  Created by daihua ye on 3/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RecommendationCell.h"
#import "CoreDataTableViewController.h"

@interface RecommendationTableViewController : CoreDataTableViewController
{
    RecommendationCell *tmpCell;
    UINib *cellNib;
}

@property (nonatomic, strong) UIManagedDocument *recommendationDatabase;  // Model is Core Data database of Recommendations

@property (retain, nonatomic) IBOutlet RecommendationCell *tmpCell;
@property (nonatomic, retain) UINib *cellNib;

@end
