//
//  StoreDetailViewController.m
//  GoShop
//
//  Created by daihua ye on 3/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "StoreDetailViewController.h"



@implementation StoreDetailViewController
@synthesize address, phoneNumber, website;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    //return YES;
    return NO;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    if (section == 0) {
        return 3;
    }
    if (section == 1) {
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    if(indexPath.section == 0) {
        if(indexPath.row == 0){
            cell.textLabel.text = @"Address";
            cell.detailTextLabel.text = address;
        }
        if(indexPath.row == 1){
            cell.textLabel.text = @"Phone Number";
            cell.detailTextLabel.text = phoneNumber;
        }
        if(indexPath.row == 2){
            cell.textLabel.text = @"Website";
            cell.detailTextLabel.text = website;
        }
    }
    if (indexPath.section == 1) {
        cell.textLabel.text = @"Store Hours";
        cell.detailTextLabel.text = @"Open 24 Hours";
    }
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0){
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    if(indexPath.row == 1){
        NSString *cleanedString = [[phoneNumber componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:phoneNumber] invertedSet]] componentsJoinedByString:@""];
        NSString *escapedPhoneNumber = [cleanedString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [[UIApplication sharedApplication] openURL: [NSURL URLWithString:[NSString stringWithFormat:@"tel://%@", escapedPhoneNumber]]];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    if(indexPath.row == 2){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:website]];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}
@end