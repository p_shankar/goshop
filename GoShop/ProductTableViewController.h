//
//  ProductTableViewController.h
//  GoShop
//
//  Created by daihua ye on 3/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Category.h"
#import "CoreDataTableViewController.h"
#import "TableViewCell.h"

@interface ProductTableViewController : CoreDataTableViewController <UISearchDisplayDelegate, UISearchBarDelegate> {
	NSArray	*listContent;			// The master content.
	NSMutableArray	*filteredListContent;	// The content filtered as a result of a search.
	
	// The saved state of the search UI if a memory warning removed the view.
    NSString		*savedSearchTerm;
    NSInteger		savedScopeButtonIndex;
    BOOL			searchWasActive;    
    
    // reference to xib-based
    UINib *cellNib;
    TableViewCell *tmpCell;
}

@property (nonatomic, retain) NSArray *listContent;
@property (nonatomic, retain) NSMutableArray *filteredListContent;

@property (nonatomic, copy) NSString *savedSearchTerm;
@property (nonatomic) NSInteger savedScopeButtonIndex;
@property (nonatomic) BOOL searchWasActive;

@property (nonatomic, strong) Category *category;
@property (nonatomic, copy) UIManagedDocument * productDocument;

@property (retain, nonatomic) IBOutlet TableViewCell *tmpCell;
@property (nonatomic, retain) UINib *cellNib;

-(void) setupFetchedResultsController;
-(void) createProductsWithCategoryId:(NSNumber *)category_id inManagedDocument:(UIManagedDocument *)document ;


@end
