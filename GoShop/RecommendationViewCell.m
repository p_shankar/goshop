//
//  RecommendationViewCell.m
//  GoShop
//
//  Created by daihua ye on 3/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RecommendationViewCell.h"
#import "ASIFormDataRequest.h"

#define GOSHOP_RECOMMENDATION_POST_URL @"http://goshopucd.heroku.com/recommendations"

@interface RecommendationViewCell()

- (void)uploadFailed:(ASIHTTPRequest *)theRequest;
- (void)uploadFinished:(ASIHTTPRequest *)theRequest;

@end

@implementation RecommendationViewCell
@synthesize request = _request;

-(void)setRecommendation_message:(NSString *)new_recommendation_message {
    [super setRecommendation_message:new_recommendation_message];
    uiRecommendationMessage.text = new_recommendation_message;
}

-(void)setDeleteProductID:(NSNumber *)newDeleteProductID {
    [super setDeleteProductID:newDeleteProductID];
    
    deleteID = newDeleteProductID;
}

-(void)setImage_url:(NSString *)new_image_url {
    [super setImage_url:new_image_url];
    uiImageURL.text = new_image_url;
}

-(void)setRateFromUser:(UIImageView *)newRateFromUser {
    [super setRateFromUser:newRateFromUser];
    rateImageFromUser.image = [newRateFromUser image];
}

#pragma mark - HTTP Request Respond

- (IBAction)performPOSTRequest:(id)sender {
    [_request cancel];
    
    [performPOSTRequestButton setEnabled:NO];
    [performDELETERequestButton setEnabled:NO];
    
    [self setRequest:[ASIFormDataRequest requestWithURL:[NSURL URLWithString:GOSHOP_RECOMMENDATION_POST_URL]]];
    [_request setPostValue:@"4" forKey:@"rate"];
    [_request setPostValue:@"Hello world, my first message is post here" forKey:@"message"];
    [_request setPostValue:@"http://goshopucd.heroku.com" forKey:@"url"];
    [_request setTimeOutSeconds:20];
    
    #if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_4_0
        [_request setShouldContinueWhenAppEntersBackground:YES];
    #endif
	[_request setDelegate:self];
	[_request setDidFailSelector:@selector(uploadFailed:)];
	[_request setDidFinishSelector:@selector(uploadFinished:)];

    
    NSLog(@"request startAsynchronous");
    [_request startAsynchronous];
    //    [resultView setText:@"POST Request has been sent..."];
}


- (IBAction)performDELETERequest:(id)sender {
    [_request cancel];
    NSString *deleteUrl = [NSString stringWithFormat:@"%@/%d", GOSHOP_RECOMMENDATION_POST_URL, [deleteID intValue]];
    [self setRequest:[ASIFormDataRequest requestWithURL:[NSURL URLWithString:deleteUrl]]];
    [_request setRequestMethod:@"DELETE"];
    
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_4_0
    [_request setShouldContinueWhenAppEntersBackground:YES];
#endif
	[_request setDelegate:self];
	[_request setDidFailSelector:@selector(uploadFailed:)];
	[_request setDidFinishSelector:@selector(uploadFinished:)];
    
    NSLog(@"request startAsynchronous");
    [_request startAsynchronous];
}

- (void)uploadFailed:(ASIHTTPRequest *)theRequest
{
//    resultText = [NSString stringWithFormat:@"Request failed:\r\n%@", [[theRequest error] localizedDescription]];
    
    [performPOSTRequestButton setEnabled:YES];
    [performDELETERequestButton setEnabled:YES];
//    NSLog(@"Here is the failed %@", resultText);
    //	[resultView setText:[NSString stringWithFormat:@"Request failed:\r\n%@",[[theRequest error] localizedDescription]]];
}

- (void)uploadFinished:(ASIHTTPRequest *)theRequest {
//    resultText = [NSString stringWithFormat:@"Finish Roalding", [theRequest postLength]];
    [performPOSTRequestButton setEnabled:YES];
    [performDELETERequestButton setEnabled:YES];
//    NSLog(@"Here is the Successful %@", resultText);
    //    [resultView setText:[NSString stringWithFormat:@"Finished uploading %llu bytes of data",[theRequest postLength]]];
}

@end
