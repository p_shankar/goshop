//
//  GoShopData.h
//  GoShop
//
//  Created by daihua ye on 3/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#define GOSHOP_CATEGORY_NAME @"name"
#define GOSHOP_CATEGORY_NUMBER @"number"
#define GOSHOP_CATEGORY_ID @"id"
#define GOSHOP_PRODUCT_NAME @"name"
#define GOSHOP_PRODUCT_DESCRIPTION @"description"
#define GOSHOP_PRODUCT_ITEM_PRICE @"item_price"
#define GOSHOP_PRODUCT_SAVING @"saving"
#define GOSHOP_PRODUCT_PRICE_VALID @"price_valid"
#define GOSHOP_PRODUCT_CATEGORY_ID @"category_id"
#define GOSHOP_PRODUCT_ID @"id"
#define GOSHOP_URL @"http://goshopucd.heroku.com/"
#define GOSHOP_RECOMMENDATION_ID @"id"
#define GOSHOP_RECOMMENDATION_PRODUCT_ID @"product_id"
#define GOSHOP_RECOMMENDATION_RATE @"rate"
#define GOSHOP_RECOMMENDATION_MESSAGE @"message"
#define GOSHOP_RECOMMENDATION_URL @"url"
#define GOSHOP_RECOMMENDATION_POST_URL @"http://goshopucd.heroku.com/recommendations"

//#define GOSHOP_URL @"http://danceinweb.com/"

@interface GoShopData : NSObject

//+(NSURL *)urlForCategory:(NSDictionary *)categroy;
+ (NSArray *)recentGeoreferenced:(NSString *)keypath andQuery:(NSString *)query;

@end
