//
//  Product+Create.m
//  GoShop
//
//  Created by daihua ye on 3/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Product+Create.h"
#import "GoShopData.h"
#import "Category+Create.h"
#import "Category.h"

@implementation Product (Create)

//+(Product *)productWithProductInfo:(NSDictionary *)productInfo inManagedObjectContext:(NSManagedObjectContext *)context {
//    Product *product = nil;
//    
//    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Product"];
//    for (NSString *value in [productInfo allValues]) {
//        NSLog(@"keys are %@", value);
//    }
//
//    request.predicate = [NSPredicate predicateWithFormat:@"product_id = %@", [productInfo objectForKey:GOSHOP_PRODUCT_ID]];
//    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
//    request.sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
//    
//    NSError *error = nil;
//    NSArray *matches = [context executeFetchRequest:request error:&error];
//    
//    if (!matches || [matches count] > 1) {
//        //handle error
//    } else if ([matches count] == 0) {
//        product = [NSEntityDescription insertNewObjectForEntityForName:@"Product" inManagedObjectContext:context];
//        product.product_id = [productInfo objectForKey:GOSHOP_PRODUCT_ID];
//        product.category_id = [productInfo objectForKey:GOSHOP_PRODUCT_CATEGORY_ID];
//        product.descriptions = [productInfo objectForKey:GOSHOP_PRODUCT_DESCRIPTION];
//        product.item_price = [productInfo objectForKey:GOSHOP_PRODUCT_ITEM_PRICE];
//        product.name = [productInfo objectForKey:GOSHOP_PRODUCT_NAME];
//        product.price_valid = [productInfo objectForKey:GOSHOP_PRODUCT_PRICE_VALID];
//        product.saving = [productInfo objectForKey:GOSHOP_PRODUCT_SAVING];
//        product.whichCategory = [Category categoryWithName:product.category_id inManagedObjectContext:context];
//    } else {
//        product = [matches lastObject];
//    }
//    return product;
//}

+(Product *)createProductWithCategoryID:(NSDictionary *)productInfo andCategory:(Category *)category inManagedObjectContext:(NSManagedObjectContext *)context {
        
    Product *product = nil;
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Product"];
    
    request.predicate = [NSPredicate predicateWithFormat:@"product_id = %@", [productInfo objectForKey:GOSHOP_PRODUCT_ID]];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    request.sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    NSError *error = nil;
    NSArray *matches = [context executeFetchRequest:request error:&error];
    
    if (!matches || [matches count] > 1) {
        //handle error
    } else if ([matches count] == 0) {
        product = [NSEntityDescription insertNewObjectForEntityForName:@"Product" inManagedObjectContext:context];
        product.product_id = [productInfo objectForKey:GOSHOP_PRODUCT_ID];
        product.category_id = [productInfo objectForKey:GOSHOP_PRODUCT_CATEGORY_ID];
        product.descriptions = [productInfo objectForKey:GOSHOP_PRODUCT_DESCRIPTION];
        product.item_price = [productInfo objectForKey:GOSHOP_PRODUCT_ITEM_PRICE];
        product.name = [productInfo objectForKey:GOSHOP_PRODUCT_NAME];
        product.price_valid = [productInfo objectForKey:GOSHOP_PRODUCT_PRICE_VALID];
        product.saving = [productInfo objectForKey:GOSHOP_PRODUCT_SAVING];
        product.whichCategory = category;
    } else {
        if ([[matches lastObject] category_id] != [productInfo objectForKey:GOSHOP_PRODUCT_CATEGORY_ID]) {
            product = [NSEntityDescription insertNewObjectForEntityForName:@"Product" inManagedObjectContext:context];
            product.product_id = [productInfo objectForKey:GOSHOP_PRODUCT_ID];
            product.category_id = [productInfo objectForKey:GOSHOP_PRODUCT_CATEGORY_ID];
            product.descriptions = [productInfo objectForKey:GOSHOP_PRODUCT_DESCRIPTION];
            product.item_price = [productInfo objectForKey:GOSHOP_PRODUCT_ITEM_PRICE];
            product.name = [productInfo objectForKey:GOSHOP_PRODUCT_NAME];
            product.price_valid = [productInfo objectForKey:GOSHOP_PRODUCT_PRICE_VALID];
            product.saving = [productInfo objectForKey:GOSHOP_PRODUCT_SAVING];
            product.whichCategory = category;
        } else {
            product = [matches lastObject];
            
        }
    }
    
    return product;
}

@end
