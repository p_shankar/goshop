//
//  StoreRefViewController.h
//  GoShop
//
//  Created by Viet Trinh on 2/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "MyAnnotation.h"
#import "Store.h"
#import "DataController.h"

#define METERS_PER_MILE 1609.344

@interface StoreRefViewController : UIViewController <UISearchBarDelegate, MKMapViewDelegate>
{
    NSMutableArray* storelist;
    CLLocationCoordinate2D location;
    NSMutableArray* filteredList;
    NSString* savedSearchTerm;
    NSInteger savedScopeButtonIndex;
    BOOL searchWasActive;
    __weak IBOutlet UISearchBar* searchBar;
    double cur_value;
}
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property(nonatomic) BOOL showsUserLocation;
@property(nonatomic) CLLocationCoordinate2D location;
@property (nonatomic, retain) NSArray *listContent;
@property (nonatomic, retain) NSMutableArray *filteredListContent;
@property (nonatomic, copy) NSString *savedSearchTerm;
@property (nonatomic) NSInteger savedScopeButtonIndex;
@property (nonatomic) BOOL searchWasActive;
- (void)search:(CLLocationCoordinate2D)searchCoordinates;
- (void)mapView:(MKMapView *)mv annotationView:(MKAnnotationView *)pin calloutAccessoryControlTapped:(UIControl *)control;
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation;




@end
