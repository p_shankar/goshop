//
//  Category.m
//  GoShop
//
//  Created by daihua ye on 3/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Category.h"
#import "Product.h"


@implementation Category

@dynamic category_id;
@dynamic name;
@dynamic number;
@dynamic hasProducts;

@end
