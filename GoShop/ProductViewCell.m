//
//  ProductViewCell.m
//  GoShop
//
//  Created by daihua ye on 3/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ProductViewCell.h"

@implementation ProductViewCell
@synthesize supermarket_name, product_name, product_price, product_date, product_additional_info, product_description;

@end
