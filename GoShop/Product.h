//
//  Product.h
//  GoShop
//
//  Created by daihua ye on 3/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Category, Recommendation;

@interface Product : NSManagedObject

@property (nonatomic, retain) NSNumber * category_id;
@property (nonatomic, retain) NSString * descriptions;
@property (nonatomic, retain) NSString * item_price;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * price_valid;
@property (nonatomic, retain) NSNumber * product_id;
@property (nonatomic, retain) NSString * saving;
@property (nonatomic, retain) Category *whichCategory;
@property (nonatomic, retain) NSSet *hasRecommendations;
@end

@interface Product (CoreDataGeneratedAccessors)

- (void)addHasRecommendationsObject:(Recommendation *)value;
- (void)removeHasRecommendationsObject:(Recommendation *)value;
- (void)addHasRecommendations:(NSSet *)values;
- (void)removeHasRecommendations:(NSSet *)values;

@end
