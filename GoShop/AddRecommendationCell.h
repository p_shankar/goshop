//
//  AddRecommendationCell.h
//  GoShop
//
//  Created by daihua ye on 3/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddRecommendationCell : UITableViewCell
{
    NSString *message;
    NSNumber *product_id;
    NSNumber *rate;
    NSString *url;
}

@property (retain) NSString *message;
@property (retain) NSNumber *product_id;
@property (retain) NSNumber *rate;
@property (retain) NSString *url;
@end
