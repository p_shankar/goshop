//
//  Store.m
//  GoShop
//
//  Created by Ricardo Nunez on 3/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Store.h"
@implementation Store
-(Store*)init:(NSString*)myName:(NSString*)myIdentifier:(NSString*)myStreetNumber:(NSString*)myStreetName:(NSString*)myCity:(NSString*)myAddress:(NSString*)myZipCode:(float)myLatitude:(float)myLongitude:(NSString*)myPhoneNumber:(NSURL*)myWebsite{
    name = myName;
    identifier = myIdentifier;
    streetNumber = myStreetNumber;
    streetName = myStreetName;
    city = myCity;
    zipCode = myZipCode;
    latitude = myLatitude;
    longitude = myLongitude;
    phoneNumber = myPhoneNumber;
    website = myWebsite;
    return self;
}
@synthesize name, identifier, streetNumber, streetName, city, zipCode, latitude, longitude, phoneNumber, website;
@end
