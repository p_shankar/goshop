//
//  Recommendation+Create.m
//  GoShop
//
//  Created by daihua ye on 3/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Recommendation+Create.h"
#import "GoShopData.h"
#import "Recommendation.h"

@implementation Recommendation (Create)

+(Recommendation *)createRecommendationWithInfo:(NSDictionary *)recommendationInfo inManagedObjectContext:(NSManagedObjectContext *)context {
    Recommendation *recommendation = nil;
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Recommendation"];
    
    request.predicate = [NSPredicate predicateWithFormat:@"recommendation_id == %@", [recommendationInfo objectForKey:GOSHOP_RECOMMENDATION_ID]];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"recommendation_id" ascending:NO];
    request.sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    NSError *error = nil;
    NSArray *matches = [context executeFetchRequest:request error:&error];
    
    if (!matches || [matches count] > 1) {
        // handle error
    } else if ([matches count] == 0 && ([recommendationInfo objectForKey:GOSHOP_RECOMMENDATION_MESSAGE] != (id)[NSNull null])) {
        recommendation = [NSEntityDescription insertNewObjectForEntityForName:@"Recommendation" inManagedObjectContext:context];
        recommendation.recommendation_id = [recommendationInfo objectForKey:GOSHOP_RECOMMENDATION_ID];
        recommendation.rate = [recommendationInfo objectForKey:GOSHOP_RECOMMENDATION_RATE];
        recommendation.message = [recommendationInfo objectForKey:GOSHOP_RECOMMENDATION_MESSAGE];
        if ([recommendationInfo objectForKey:GOSHOP_RECOMMENDATION_PRODUCT_ID] != (id)[NSNull null]) {
            recommendation.product_id = [recommendationInfo objectForKey:GOSHOP_RECOMMENDATION_PRODUCT_ID];
        }
        recommendation.image_url = [recommendationInfo objectForKey:GOSHOP_RECOMMENDATION_URL];
    } else {
        recommendation = [matches lastObject];
    }
    
    return recommendation;
}

@end