//
//  ProductDetailViewCell.h
//  GoShop
//
//  Created by daihua ye on 3/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductViewCell.h"

@interface ProductDetailViewCell : ProductViewCell
{
    IBOutlet UILabel *supermarketForProduct;
    IBOutlet UITextView *nameForProduct;
    IBOutlet UILabel *priceForProduct;
    IBOutlet UILabel *dateForProduct;
    IBOutlet UITextView *additionalInfoForProduct;
    IBOutlet UITextView *descriptionForProduct;
}
@end
