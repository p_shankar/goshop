//
//  RecommendationCell.h
//  GoShop
//
//  Created by daihua ye on 3/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecommendationCell : UITableViewCell
{
    NSString *recommendation_message;
    NSString *image_url;
    NSNumber *deleteProductID;
    UIImageView *rateFromUser;
}

@property (retain) NSString *recommendation_message;
@property (retain) NSNumber *deleteProductID;
@property (retain) NSString *image_url;
@property (retain) UIImageView *rateFromUser;

@end
