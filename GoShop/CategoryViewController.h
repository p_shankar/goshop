//
//  CategoryViewController.h
//  GoShop
//
//  Created by Viet Trinh on 2/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreDataTableViewController.h"
#import "LoadingScreen.h"
#import "CoordinatingViewController.h"

@class CategoryViewController;

@protocol CategoryViewControllerDelegate

@optional
-(void)categoryViewController:(CategoryViewController *)sender
     inManagedDocumentContext:(UIManagedDocument *)database;

@end

@interface CategoryViewController : CoreDataTableViewController

{
    IBOutlet LoadingScreen* loadScreen;
}

@property (nonatomic, strong) UIManagedDocument *productDatabase;  // Model is Core Data database of Products

@property (nonatomic, weak) id <CategoryViewControllerDelegate> delegate;

- (void) useDocument;
- (void) setupFetchedResultsController;
- (void) fetchServerDataIntoDocument:(UIManagedDocument *)document;

@end
