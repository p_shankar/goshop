//
//  RecommendationViewCell.h
//  GoShop
//
//  Created by daihua ye on 3/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RecommendationCell.h"

@class ASIFormDataRequest;

@interface RecommendationViewCell : RecommendationCell
{
    ASIFormDataRequest *request;
    
    IBOutlet UITextView *uiRecommendationMessage;
    IBOutlet UITextView *uiImageURL;
    
    IBOutlet UIButton *performPOSTRequestButton;
    IBOutlet UIButton *performDELETERequestButton;
    
    IBOutlet UIImageView *rateImageFromUser;
//    NSString *resultText;
    
    NSNumber *deleteID;
}


@property (strong, nonatomic) ASIFormDataRequest *request;

@end
