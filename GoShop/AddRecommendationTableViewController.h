//
//  AddRecommendationTableViewController.h
//  GoShop
//
//  Created by daihua ye on 3/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddRecommendationCell.h"
#import "RateView.h"

@class ASIFormDataRequest;


@interface AddRecommendationTableViewController : UITableViewController <UITextViewDelegate, RateViewDelegate>
{
    AddRecommendationCell *tmpCell;
    UINib *cellNib;
    
    ASIFormDataRequest *request;
    
    NSArray *tmpArray;
}

@property (retain, nonatomic) IBOutlet AddRecommendationCell *tmpCell;
@property (nonatomic, retain) UINib *cellNib;
@property (strong, nonatomic) NSArray *tmpArray;

@property (strong, nonatomic) ASIFormDataRequest *request;

@property (weak, nonatomic) IBOutlet UITextView *editView;
@property (weak, nonatomic) IBOutlet RateView *rateView;
@property (strong, nonatomic) NSString *message;
@property (strong, nonatomic) NSNumber *rateForProduct;
@end
