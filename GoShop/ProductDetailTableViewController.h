//
//  ProductDetailTableViewController.h
//  GoShop
//
//  Created by daihua ye on 3/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductViewCell.h"
#import "Product.h"

@interface ProductDetailTableViewController : UITableViewController
{
    ProductViewCell *tmpCell;
    UINib *cellNib;
    
    Product *product;
}

@property (retain, nonatomic) IBOutlet ProductViewCell *tmpCell;
@property (nonatomic, retain) UINib *cellNib;

@property (nonatomic, strong) Product *product;
@end
