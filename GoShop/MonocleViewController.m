//
//  MonocleViewController.m
//  GoShop
//
//  Created by Viet Trinh on 3/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MonocleViewController.h"
#import "ARGeoCoordinate.h"
#import "StoreRefViewController.h"
@interface MonocleViewController ()

@end

@implementation MonocleViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
//    self.view.backgroundColor = [UIColor blueColor];
    
    UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
    }
    [imagePicker setDelegate:self];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        imagePickerPopover = [[UIPopoverController alloc] initWithContentViewController:imagePicker];
        [imagePickerPopover setDelegate:self];
        [imagePickerPopover presentPopoverFromBarButtonItem:nil permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }else{
        [self presentModalViewController:imagePicker animated:YES];
    }
    //imagePicker.view.hidden = YES;
    [imagePicker release];
}

-(void)viewDidAppear:(BOOL)animated{
    ARGeoViewController *viewController = [[ARGeoViewController alloc] init];
	viewController.debugMode = YES;
	
	viewController.delegate = self;
	
	viewController.scaleViewsBasedOnDistance = YES;
	viewController.minimumScaleFactor = .5;
	
	viewController.rotateViewsBasedOnPerspective = YES;
	
	NSMutableArray *tempLocationArray = [[NSMutableArray alloc] initWithCapacity:10];
	
	CLLocation *tempLocation;
	ARGeoCoordinate *tempCoordinate;
	
	CLLocationCoordinate2D location;
	location.latitude = 38.5609694;
    location.longitude = -121.7662882;
    
	tempLocation = [[CLLocation alloc] initWithCoordinate:location altitude:1609.0 horizontalAccuracy:1.0 verticalAccuracy:1.0 timestamp:[NSDate date]];
	
	tempCoordinate = [ARGeoCoordinate coordinateWithLocation:tempLocation];
	tempCoordinate.title = @"SafeWay 1";
    tempCoordinate.subtitle = @"1451 W Covell Blvd, Davis, CA";
	[tempLocationArray addObject:tempCoordinate];
	[tempLocation release];
	
	tempLocation = [[CLLocation alloc] initWithLatitude:40.541297 longitude:-121.724851];
	tempCoordinate = [ARGeoCoordinate coordinateWithLocation:tempLocation];
	tempCoordinate.title = @"SafeWay 2";
    tempCoordinate.subtitle = @"2121 Cowell, Davis, CA";
    [tempLocationArray addObject:tempCoordinate];
	[tempLocation release];

	tempLocation = [[CLLocation alloc] initWithLatitude:35.4578888 longitude:-124.9393805];
	tempCoordinate = [ARGeoCoordinate coordinateWithLocation:tempLocation];
	tempCoordinate.title = @"SafeWay 3";
    tempCoordinate.subtitle = @"1235 Stratford Ave, Dixon, CA";
	[tempLocationArray addObject:tempCoordinate];
	[tempLocation release];
	
	
		
	[viewController addCoordinates:tempLocationArray];
	[tempLocationArray release];
    
	CLLocation *newCenter = [[CLLocation alloc] initWithLatitude:38.41711 longitude:-121.02528];
	
	viewController.centerLocation = newCenter;
	[newCenter release];
	
	[viewController startListening];
    [self.view addSubview:viewController.view];
    

}

- (void)viewDidUnload
{
//    [self setCloseMonocle:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.

}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    //return YES;
    return NO;
}

#define BOX_WIDTH 150
#define BOX_HEIGHT 100

- (UIView *)viewForCoordinate:(ARCoordinate *)coordinate {
	
	CGRect theFrame = CGRectMake(0, 0, BOX_WIDTH, BOX_HEIGHT);
	UIView *tempView = [[UIView alloc] initWithFrame:theFrame];
	
//	tempView.backgroundColor = [UIColor blueColor];
    
    UILabel *addressLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, BOX_WIDTH, 40.0)];
    addressLabel.backgroundColor = [UIColor colorWithWhite:.3 alpha:.8];
	addressLabel.textColor = [UIColor whiteColor];
	addressLabel.textAlignment = UITextAlignmentCenter;
	addressLabel.text = coordinate.subtitle;
    [addressLabel sizeToFit];
    addressLabel.frame = CGRectMake(BOX_WIDTH / 2.0 - addressLabel.frame.size.width / 2.0 - 4.0, addressLabel.frame.size.height+8.0, addressLabel.frame.size.width + 8.0, addressLabel.frame.size.height + 8.0);
	
	UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, BOX_WIDTH, 40.0)];
	titleLabel.backgroundColor = [UIColor colorWithWhite:.3 alpha:.8];
	titleLabel.textColor = [UIColor whiteColor];
	titleLabel.textAlignment = UITextAlignmentCenter;
	titleLabel.text = coordinate.title;
    [titleLabel sizeToFit];
	titleLabel.frame = CGRectMake(BOX_WIDTH / 2.0 - titleLabel.frame.size.width / 2.0, 0, titleLabel.frame.size.width + 8.0, titleLabel.frame.size.height + 8.0);
	
    
    
	UIImageView *pointView = [[UIImageView alloc] initWithFrame:CGRectZero];
	pointView.image = [UIImage imageNamed:@"shoppingcart.png"];
	pointView.frame = CGRectMake((int)(BOX_WIDTH / 2.0 - pointView.image.size.width / 2.0), (int)(BOX_HEIGHT / 2.0 - pointView.image.size.height / 2.0 + addressLabel.frame.size.height), pointView.image.size.width, pointView.image.size.height);
    
	[tempView addSubview:titleLabel];
    [tempView addSubview:addressLabel];
	[tempView addSubview:pointView];
	
	[titleLabel release];
    [addressLabel release];
	[pointView release];
	
	return [tempView autorelease];
}



@end
