//
//  ProductDetailViewCell.m
//  GoShop
//
//  Created by daihua ye on 3/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ProductDetailViewCell.h"

@implementation ProductDetailViewCell

-(void)setSupermarket_name:(NSString *)newName {
    [super setSupermarket_name:newName];
    supermarketForProduct.text = newName;
}

-(void)setProduct_name:(NSString *)new_product_name {
    [super setProduct_name:new_product_name];
    nameForProduct.text = new_product_name;
}

-(void)setProduct_price:(NSString *)new_product_price {
    [super setProduct_price:new_product_price];
    priceForProduct.text = new_product_price;
}

-(void)setProduct_date:(NSString *)new_product_date {
    [super setProduct_date:new_product_date];
    dateForProduct.text = new_product_date;
}

-(void)setProduct_additional_info:(NSString *)new_product_additional_info {
    [super setProduct_additional_info:new_product_additional_info];
    additionalInfoForProduct.text = new_product_additional_info;
}

-(void)setProduct_description:(NSString *)new_product_description {
    [super setProduct_description:new_product_description];
    descriptionForProduct.text = new_product_description;
}

@end
