//
//  Product+Create.h
//  GoShop
//
//  Created by daihua ye on 3/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Product.h"

@interface Product (Create)

//+(Product *)productWithProductInfo:(NSDictionary *)productInfo inManagedObjectContext:(NSManagedObjectContext *)context;
+(Product *)createProductWithCategoryID:(NSDictionary *)productInfo andCategory:(Category *)category inManagedObjectContext:(NSManagedObjectContext *)context;

@end
