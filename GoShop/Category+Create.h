//
//  Category+Create.h
//  GoShop
//
//  Created by daihua ye on 3/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Category.h"

@interface Category (Create)

//+ (Category *)categoryWithName:(NSNumber *)category_id
//                inManagedObjectContext:(NSManagedObjectContext *)context;
+(Category *)createCategoryWithCategories:(NSDictionary *)category inManagedObjectContext:(NSManagedObjectContext *)context;

@end
