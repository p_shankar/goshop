#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "Store.h"
@interface MyAnnotation : NSObject<MKAnnotation>{
    CLLocationCoordinate2D coordinate;
    NSString* title;
    NSString* subtitle;
    Store* store;
}
-(MyAnnotation*) init:(CLLocationCoordinate2D)myCoordinate:(NSString*)myTitle:(NSString*)mySubtitle:(Store*)myStore;
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString* title;
@property (nonatomic, copy) NSString* subtitle;
@property (nonatomic, copy) Store* store;
@end
