//
//  ProductViewCell.h
//  GoShop
//
//  Created by daihua ye on 3/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductViewCell : UITableViewCell
{
    NSString *supermarket_name;
    NSString *prdouct_name;
    NSString *product_price;
    NSString *product_date;
    NSString *product_additional_info;
    NSString *product_description;
}

@property (retain) NSString *supermarket_name;
@property (retain) NSString *product_name;
@property (retain) NSString *product_price;
@property (retain) NSString *product_date;
@property (retain) NSString *product_additional_info;
@property (retain) NSString *product_description;

@end
