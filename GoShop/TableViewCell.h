//
//  TableViewCell.h
//  GoShop
//
//  Created by daihua ye on 3/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewCell : UITableViewCell
{
    BOOL useDarkBackground;
    NSString *name;
    NSString *price;
    NSString *date;
    NSString *company;
}

@property BOOL useDarkBackground;
@property (retain) NSString * name;
@property (retain) NSString *date;
@property (retain) NSString *price;
@property (retain) NSString *company;

@end
