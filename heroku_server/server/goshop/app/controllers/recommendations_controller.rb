class RecommendationsController < ApplicationController
  respond_to :json, :xml
  
  # GET /recommendations
  # GET /recommendations.json
  def index
    if params[:product_id]
      @recommendations = Recommendation.find_all_by_product_id(params[:product_id])
    else
      num = 10
      if params[:limit_num]
        num = params[:limit_num]
      end
      @recommendations = Recommendation.order("id DESC").limit(num)
    end
  end

  # GET /recommendations/1
  # GET /recommendations/1.json
  def show
    @recommendation = Recommendation.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @recommendation }
    end
  end
  
  # def create_recommendations
  #   if params[:recommendation]
  #     @recommendation = Recommendation.new(params[:recommendation])
  #   end
  #   
  # end
  
  # GET /recommendations/new
  # GET /recommendations/new.json
  def new
    @recommendation = Recommendation.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @recommendation }
    end
  end

  # GET /recommendations/1/edit
  def edit
    @recommendation = Recommendation.find(params[:id])
  end

  # POST /recommendations
  # POST /recommendations.json
  def create
    if params[:message] && !params[:product_id]
      @recommendation = Recommendation.new(:message => params[:message], :rate => params[:rate].to_i, :url => params[:url])
    elsif params[:product_id]
      @recommendation = Recommendation.new(:message => params[:message], :rate => params[:rate].to_i, :url => params[:url], :product_id => params[:product_id])
    end
    

    respond_to do |format|
      if @recommendation.save
        format.html { redirect_to @recommendation, notice: 'Recommendation was successfully created.' }
        format.json { render json: @recommendation, status: :created, location: @recommendation }
      else
        format.html { render action: "new" }
        format.json { render json: @recommendation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /recommendations/1
  # PUT /recommendations/1.json
  def update
    @recommendation = Recommendation.find(params[:id])

    respond_to do |format|
      if @recommendation.update_attributes(params[:recommendation])
        format.html { redirect_to @recommendation, notice: 'Recommendation was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @recommendation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /recommendations/1
  # DELETE /recommendations/1.json
  def destroy
    @recommendation = Recommendation.find(params[:id])
    @recommendation.destroy

    respond_to do |format|
      format.html { redirect_to recommendations_url }
      format.json { head :ok }
    end
  end
end
