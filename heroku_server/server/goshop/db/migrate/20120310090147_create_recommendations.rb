class CreateRecommendations < ActiveRecord::Migration
  def change
    create_table :recommendations do |t|
      t.integer :product_id
      t.integer :rate
      t.text :message
      t.string :url

      t.timestamps
    end
  end
end
