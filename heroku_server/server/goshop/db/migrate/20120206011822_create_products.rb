class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.text    :description
      t.string :item_price
      t.text  :saving
      t.string :price_valid
      t.integer :category_id
      
      t.timestamps
    end
  end
end
