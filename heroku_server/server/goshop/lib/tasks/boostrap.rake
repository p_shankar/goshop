require 'httparty'
require 'pp'
require 'nokogiri'

class HtmlParserIncluded < HTTParty::Parser
  SupportedFormats.merge!('text/html' => :html)

  # Instance Attribute body: -(string) body readonly the response body of the request
  # using Nokogiri, is an HTML, XML parser. which can parses and searches XML/HTML very quickly
  # and also has correctly implemented css3 selector supprot as well as XPATH support
  def html
    Nokogiri::HTML(body)
  end
end

class Page
  include HTTParty
  parser HtmlParserIncluded
end

# serach tutorial
####
# Search for nodes by css
# doc.css('h3.r a.l').each do |link|
# puts link.content
# end
# 
# ####
# # Search for nodes by xpath
# doc.xpath('//h3/a[@class="l"]').each do |link|
# puts link.content
# end
# 
# ####
# # Or mix and match.
# doc.search('h3.r a.l', '//h3/a[@class="l"]').each do |link|
# puts link.content
# end

namespace :goshop do
  desc "get data from html"
  task :category => :environment do
    url = 'http://goshopucd.heroku.com/2-26-2012/Category_1431_covell.html'
    response = HTTParty.get(url)
    
    puts "delete all data in Category"
    Category.destroy_all
   
    if response.code == 200
      @doc = Page.get(url)
      @doc.xpath('//div/span/li/a').each do |node|
        name =  node.text.match(/[^(*)$]*[a-z$]/).to_s
        num =  node.text.match(/([0-9][0-9]|[0-9])/).to_s.to_i
        category = Category.new
        category.update_attributes(:name => name.strip, :number => num)
      end
      puts "successfully create category"
    end
  end
  
  desc "save data for each category"
  task :product => :environment do
    puts "destroy all the data"
    Product.destroy_all
    
    puts "starting to store data"
    @categories = Category.all
    @categories.each do |category|
      category_name = category.name.gsub(/[&, ]/, '')
      url = "http://goshopucd.heroku.com/2-26-2012/#{category_name}.html"
      response = HTTParty.get(url)

      i = 0
      if response.code == 200
        @doc = Page.get(url)
        data_ary = []
        @doc.xpath('//td[@class="slpreviewtxt normal"]').each do |node|
          item_data = node.text.match(/[^\s].*[a-zA-Z0-9$]/).to_s
          if i < 5
            data_ary << item_data
          else
            if data_ary != []
              product = Product.new
              product.update_attributes(:name => data_ary[0], :description => data_ary[1], :item_price => data_ary[2], :saving => data_ary[3], :price_valid => data_ary[4], :category_id => category.id)
            end
            data_ary = []
          end
          i = (i + 1) % 7
        end
      end
      puts "successfully create #{category_name}"
    end
  end
end