/*
 * GET home page.
 */
var jsdom = require('jsdom')
  , request = require('request');

// var safeway_95616 = 'http://weeklyspecials.safeway.com/availableAds.jsp?drpStoreID=1205'
var safeway_95616 = 'http://dygames.site11.com/goshop/data_from_safeway/Category_1431_covell.html'
exports.index = function(req, res){
   request({url: safeway_95616}, function(err, response, body){
      if (err && response.statusCode !== 200) { console.log('Request error! Not HTTP 200'); };
      jsdom.env({
         html: body,
         scripts: ['http://code.jquery.com/jquery-1.6.min.js']
      }, function(err, window) {
		var $ = window.jQuery,
			$safeway = $('html');
		
		console.log($safeway);
        res.render('index', {
            title: 'Go! Shop'
         });
      });
   });
};
