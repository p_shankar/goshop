
/**
 * Copyright 2011-2012 Daihua Ye
 */

/**
 *Module dependencies.
 */
var express = require('express')
  , routes = require('./routes')
  , jsdom = require('jsdom')
  , request = require('request')
  , url = require('url')
  , app = module.exports = express.createServer();

// Configuration

app.configure(function(){
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(__dirname + '/public'));
});

app.configure('development', function(){
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true })); 
});

app.configure('production', function(){
  app.use(express.errorHandler()); 
});

//Uitility functions
//trim all the empty string in the string
if(typeof(String.prototype.trim) === "undefined")
{
    String.prototype.trim = function() 
    {
        return String(this).replace(/^\s+|\s+$/g, '');
    };
}
if(typeof(String.prototype.strip_tag) === "undefined")
{
   String.prototype.strip_tag = function()
   {
      return String(this).replace(/(<([^>]+)>)/ig,"");
   }
}

//Start Hacking 
app.get('/', routes.index);

app.get('/goshop', function(req, res) {
	// var request_url = 'http://weeklyspecials.safeway.com/main_508.jsp?drpStoreID=1205';
	request_url = 'http://dygames.site11.com/goshop/data_from_safeway/Category_1431_covell.html';
	request({url: request_url}, function(err, response, body) {
		if (err && response.statusCode !== 200) { console.log('Request error! Not HTTP 200'); };
		jsdom.env({
         	html: body,
         	scripts: ['http://code.jquery.com/jquery-1.6.min.js']
      	}, function(err, window) {
			console.log("hello world");
			// res.json(null);
			res.render('safeway', {
				            title: 'safeway'
				         });
	  	});
	});
});

app.get('/safeway', function(req, res) {
	var request_url = 'http://weeklyspecials.safeway.com/main_508.jsp?drpStoreID=1205';
	request({url: request_url}, function(err, response, body) {
		if (err && response.statusCode !== 200) { console.log('Request error! Not HTTP 200'); };
		jsdom.env({
         	html: body,
         	scripts: ['http://code.jquery.com/jquery-1.6.min.js']
      	}, function(err, window) {
			var $ = window.jQuery,
				$ads_list = $('#globalContainer .aastore').text();
			console.log("hello world");
			console.log($('html'));
			res.render('safeway', {
	            title: 'safeway'
	         });
	  	});
	});
});

var port = process.env.PORT || 3000;
app.listen(port, function() {
   console.log("Listening on " + port);
});
console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);